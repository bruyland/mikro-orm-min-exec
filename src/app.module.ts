import { MikroOrmModule, MikroOrmModuleSyncOptions } from '@mikro-orm/nestjs'
import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { RequestOnlineTest } from './entities/request-online-testen.entity'
import { RequestOnlineTube } from './entities/request-online-tube.entity'
import { RequestOnline } from './entities/request-online.entity'

@Module({
  imports: [
    MikroOrmModule.forRootAsync({ useFactory: ormFactory }),
    MikroOrmModule.forFeature([RequestOnline, RequestOnlineTube, RequestOnlineTest]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

async function ormFactory(): Promise<MikroOrmModuleSyncOptions> {
  return {
    entities: [RequestOnline, RequestOnlineTube, RequestOnlineTest],
    dbName: 'rsa',
    host: 'rsa-db-test.bruyland.be',
    user: 'mikro',
    password: 'mikroORM',
    type: 'mariadb',
  }
}
