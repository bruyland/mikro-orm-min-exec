import { Entity, Index, ManyToOne, OneToMany, PrimaryKey, Property } from '@mikro-orm/core'
import { RequestOnline } from './request-online.entity'

@Entity({ tableName: 'mlb_request_online_tubes' })
export class RequestOnlineTube {
  @ManyToOne('RequestOnline' /* => RequestOnline /*{ entity: () => RequestOnline }*/)
  request!: RequestOnline

  @PrimaryKey({ length: 17, default: '' })
  requestnumber!: string

  @Index({ name: 'tube' })
  @PrimaryKey({ length: 12, default: '' })
  tube!: string

  @Property({ length: 35, default: '' })
  gescand!: string

  @Property({ length: 20, default: '' })
  kode!: string

  @Property({ columnType: 'enum', default: 'J' })
  actief!: string

  @Property({ length: 10, default: '' })
  commentaar!: string

  @Property()
  created!: Date

  @Property({ fieldName: 'originId', length: 5, nullable: true, defaultRaw: `NULL` })
  originId?: string

  @Property({ fieldName: 'specimenTypeId', length: 8, nullable: true, defaultRaw: `NULL` })
  specimenTypeId?: string

  @Property({ fieldName: 'sentToLisAt', nullable: true, defaultRaw: `NULL` })
  sentToLisAt?: Date

  @Property({ fieldName: 'sentToLisBy', length: 11, default: '' })
  sentToLisBy!: string

  // @ManyToOne({ entity: () => RequestOnline })
  // @ManyToOne(() => RequestOnline)
  // ordernumber!: RequestOnline
}
