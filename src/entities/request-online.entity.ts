import { Entity, Filter, Index, OneToMany, PrimaryKey, Property, Collection } from '@mikro-orm/core'
import { RequestOnlineTube } from './request-online-tube.entity'

@Entity({ tableName: 'mlb_request_online' })
@Index({ name: 'doctor', properties: ['doctor', 'created', 'dossier'] })
@Filter({ name: 'patientNameFilter', cond: args => ({ patientname: { $like: '%' + args.name + '%' } }) })
@Filter({ name: 'doctorFilter', cond: args => ({ doctor: { $in: args.doctorArray } }) })
@Filter({ name: 'animalFilter', cond: args => ({ dier: 'J' }) })
export class RequestOnline {
  @OneToMany(() => RequestOnlineTube, tube => tube.request)
  tubes = new Collection<RequestOnlineTube>(this)

  @PrimaryKey({ length: 17, default: '' })
  requestnumber!: string

  @Property({ length: 20, default: '' })
  doctor: string = ''

  @Property({ length: 20, default: '' })
  status: string = 'filled'

  @Property({ default: '1970-01-01 00:00:00' })
  created: Date = new Date(1970, 0, 1, 0, 0, 0)

  @Property({ default: '1970-01-01 00:00:00' })
  modified: Date = new Date(1970, 0, 1, 0, 0, 0)

  @Index({ name: 'mlb_request_online_dossier_IDX' })
  @Property({ length: 20, default: '' })
  dossier!: string

  @Property({ length: 11, default: '' })
  rrn: string = ''

  @Property({ length: 200, default: '' })
  opmerkingen: string = ''

  @Property({ length: 255, default: '' })
  kopij: string = ''

  @Property({ length: 30, default: '' })
  snelheid: string = 'N'

  @Property({ length: 30, default: '' })
  communicatie: string = ''

  @Property({ length: 50, default: '' })
  patientname: string = ''

  @Property({ length: 50, default: '' })
  patientfname: string = ''

  @Property({ length: 10, default: '' })
  geboortedatum: string = ''

  @Property({ length: 1, columnType: 'char', default: '' })
  geslacht: string = 'U'

  @Property({ length: 50, default: '' })
  straat: string = ''

  @Property({ length: 10, default: '' })
  zip: string = ''

  @Property({ length: 50, default: '' })
  woonplaats: string = ''

  @Property({ length: 20, default: '' })
  mutualiteit: string = ''

  @Property({ length: 20, default: '' })
  inschrijvingsnummer: string = ''

  @Property({ length: 20, default: '' })
  codeg: string = ''

  @Property({ length: 1, columnType: 'char', default: '' })
  tarificatie: string = ''

  @Property({ length: 25, default: '' })
  chipnr: string = ''

  @Property({ length: 15, default: '' })
  refext: string = ''

  @Property({ length: 20, default: '' })
  ras: string = ''

  @Property({ length: 20, default: '' })
  soort: string = ''

  @Property({ fieldName: 'BTWnr', length: 14, default: '' })
  BTWnr: string = ''

  @Property({ columnType: 'enum', default: 'N' })
  dier!: string

  @Property({ length: 35, default: '' })
  owner: string = ''

  @Property({ length: 20, default: '' })
  land: string = ''

  @Property({ length: 1, columnType: 'char', default: '' })
  factuur: string = ''

  @Property({ length: 1, columnType: 'char', default: '' })
  gesteriliseerd: string = ''

  @Property({ length: 10, default: 'ideos' })
  bron: string = 'uitpak'

  @Property({ columnType: 'text', length: 65535 })
  anamnese: string = ''

  @Property()
  bwaarde: number = 0

  @Property({ columnType: 'double' })
  prijs: number = 0

  @Property({ columnType: 'double' })
  prijsextra: number = 0

  @Property({ length: 1, default: 'J' })
  resultatenserver: string = 'N'

  @Property({ length: 100 })
  bestand: string = ''

  @Property({ fieldName: 'tePrinten', nullable: true })
  tePrinten?: boolean = true

  @Property({ fieldName: 'sentToLisAt', nullable: true, defaultRaw: `NULL` })
  sentToLisAt: Date | null = null

  @Property({ fieldName: 'sentToLisBy', length: 100, nullable: true, defaultRaw: `NULL` })
  sentToLisBy: string | null = null

  @Property({ fieldName: 'sentToLisCount', nullable: true })
  sentToLisCount?: number = 0

  @Property({ fieldName: 'sentToLisFileName', length: 100, nullable: true, defaultRaw: `NULL` })
  sentToLisFileName: string = ''

  @Property({ columnType: 'text', fieldName: 'klinischeGegevens', length: 65535, nullable: true, defaultRaw: `NULL` })
  klinischeGegevens: string = ''

  @Property({ columnType: 'text', fieldName: 'opmerkingVeld', length: 65535, nullable: true, defaultRaw: `NULL` })
  opmerkingVeld: string = ''

  @Property({ fieldName: 'antibioticaGestartVoorAfname' })
  antibioticaGestartVoorAfname: boolean = false

  // @OneToMany(() => RequestOnlineTube, sample => sample.ordernumber)
  // @OneToMany(() => RequestOnlineTube, sample => sample.requestnumber)
}
