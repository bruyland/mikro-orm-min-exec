import { Entity, Index, PrimaryKey, Property } from '@mikro-orm/core'

@Entity({ tableName: 'mlb_request_online_testen' })
export class RequestOnlineTest {
  @Index({ name: 'requestnumber' })
  @PrimaryKey({ length: 17, default: '0' })
  requestnumber!: string

  @Property({ length: 20, default: '' })
  autonumber!: string

  @Property()
  testnumber: number = 0

  @Property({ length: 1, columnType: 'char', default: '' })
  bron!: string

  @Property({ length: 100, default: '' })
  naamtest!: string

  @Property({ length: 20, default: '' })
  code!: string

  @PrimaryKey({ length: 20, default: '' })
  exportcode!: string

  @Property({ columnType: 'enum', default: 'N' })
  afnametijd!: string

  @Property({ columnType: 'enum', default: 'N' })
  verplicht!: string

  @Property({ columnType: 'enum', default: 'N' })
  gekleurd!: string

  @Property({ length: 10, default: '' })
  kleurtube!: string

  @Property({ length: 20, default: '' })
  status!: string

  @Property({ columnType: 'enum', default: 'N' })
  extraUur!: string

  @Property({ length: 20, default: '' })
  extraUurWaarde!: string

  @Property({ columnType: 'enum', default: 'N' })
  extraMedicatie!: string

  @Property({ length: 100, default: '' })
  extraMedicatieWaarde!: string

  @Property({ columnType: 'enum', default: 'N' })
  extraKlingeg!: string

  @Property({ length: 250, default: '' })
  extraKlingegWaarde!: string

  @Property({ columnType: 'enum', default: 'N' })
  extraAardstaal!: string

  @Property({ length: 100, default: '' })
  extraAardstaalWaarde!: string

  @Property({ columnType: 'enum', default: 'N' })
  extraGewicht!: string

  @Property({ length: 10, default: '' })
  extraGewichtWaarde!: string

  @PrimaryKey()
  nummer: number = 1
}
