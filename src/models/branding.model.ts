// uses "branded types"

type Brand<K, T> = K & { __brand: T }

type OrderNumber = Brand<string, 'orderNumber'>
type RequestNumber = Brand<string, 'RequestNumber'>
type Barcode = Brand<string, 'Barcode'>
type SampleTypeId = Brand<string, 'SampleTypeId'>
type OriginId = Brand<string, 'OriginId'>
type UserId = Brand<string, 'UserId'>
type BarcodeMatchId = Brand<string, 'BarcodeMatchId'>

export { OrderNumber, RequestNumber, Barcode, SampleTypeId, OriginId, UserId, BarcodeMatchId }
