import { EntityRepository } from '@mikro-orm/core'
import { InjectRepository } from '@mikro-orm/nestjs'
import { Injectable } from '@nestjs/common'
import { RequestOnlineTube } from './entities/request-online-tube.entity'
import { RequestOnline } from './entities/request-online.entity'

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(RequestOnlineTube)
    private readonly _sampleRepository: EntityRepository<RequestOnlineTube>,
    @InjectRepository(RequestOnline)
    private readonly _requestRepository: EntityRepository<RequestOnline>,
  ) {}

  async search() {
    try {
      const result = await this._requestRepository.find(
        {},
        {
          filters: {
            doctorFilter: false,
            patientNameFilter: false,
            animalFilter: false,
          },
          limit: 10,
          populate: ['tubes'],
        },
      )
      return result
    } catch (error) {
      console.log(error)
    }
  }
}
